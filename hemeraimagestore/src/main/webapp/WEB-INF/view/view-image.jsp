<%@ include file="header.jsp"%>
<%@page import="java.util.*" %>
<%@page import="hemeraimagestore.model.*" %>

<div class="row">
<br><br>
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Image Metadata Details</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="dataTable_wrapper">
				<%
				ImageMetadataModel  imageMetaData = (ImageMetadataModel)request.getAttribute("ImageMetaData");
				//String generalException = (String)request.getAttribute("error");
				/* String packageVal="";
				for(String temp: imageMetaData.getPackages()){
					packageVal=packageVal+temp;
				} */
									
%>
					<table class="table table-striped table-bordered table-hover"
						id="dataTables-example">
						<thead>
		<tr>
			<th>Has Installer</th>
			<th>Appliance Name</th>
			<th>Checksum</th>
			<th>Version</th>
			<th>Download Size</th>			
		</tr>
						</thead>
						<tbody>
	<tr>
	<td><%=imageMetaData.getHas_installer()  %></td>
	<td><%=imageMetaData.getAppliance_name()  %></td>
	<td><%=imageMetaData.getChecksum()  %></td>
	<td><%=imageMetaData.getVersion() %></td>
	<td><%=imageMetaData.getDownload_size() %></td>
	</tr>
						</tbody>
					</table>
				</div>
				<!-- /.table-responsive -->
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<%@ include file="footer.jsp"%>