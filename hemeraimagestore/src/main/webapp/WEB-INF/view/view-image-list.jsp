<%@ include file="header.jsp"%>
<%@page import="java.util.*" %>
<%@page import="hemeraimagestore.model.*" %>

<div class="row">
<br><br>
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Images List Details</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="dataTable_wrapper">
				<%
				List<ImageModel> imageModels=(List<ImageModel>)request.getAttribute("imageList"); 

				
%>
					<table class="table table-striped table-bordered table-hover"
						id="dataTables-example">
						<thead>
							<tr>
								 <th>Upload Time</th>
    <th>Appliance Name</th>
    <th>Image Id</th>
    <th>Version</th>
    <th>Download Size</th>
    <th>Status</th>
    <th>Delete</th>
    <th>View</th>
    <th>Download</th>
    <th>View Updates</th>
							</tr>
						</thead>
						<tbody>
							<%
							for(int i=0;i<imageModels.size();i++) {
							%>
							<tr>
								<td><%=imageModels.get(i).getUpload_time() %></td>
	<td><%=imageModels.get(i).getAppliance_name() %></td>
	<td><%=imageModels.get(i).getImage_id() %></td>
	<td><%=imageModels.get(i).getVersion() %></td>
	<td><%=imageModels.get(i).getDownload_size() %></td>
	<td><%=imageModels.get(i).getStatus() %></td>
	<td><a href="/delete-images/<%=imageModels.get(i).getAppliance_name() %>/<%=imageModels.get(i).getVersion() %>">Delete</a></td>
	<td><a href="/view-image?appliance_name=<%=imageModels.get(i).getAppliance_name() %>&version=<%=imageModels.get(i).getVersion() %>">View</a></td>
	<td><a href="/download-image?version=<%=imageModels.get(i).getVersion() %>&appliance_name=<%=imageModels.get(i).getAppliance_name() %>">Download</a></td>
	<td><a href="/view-image-updates?appliance_name=<%=imageModels.get(i).getAppliance_name() %>">View Updates</a></td>
							</tr>
							<%
								}
							%>
						</tbody>
					</table>
				</div>
				<!-- /.table-responsive -->
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<%@ include file="footer.jsp"%>