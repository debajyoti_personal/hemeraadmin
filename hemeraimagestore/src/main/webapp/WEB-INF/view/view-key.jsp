<%@ include file="header.jsp"%>
<%@page import="java.util.*"%>
<%@page import="hemeraimagestore.model.KeyModel"%>

<div class="row">
<br><br>
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Key List Details</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="dataTable_wrapper">
				<%
List<KeyModel>  keyModels = (List<KeyModel>)request.getAttribute("keyModelList");
String generalException = (String)request.getAttribute("error");
if(keyModels != null) {
%>
					<table class="table table-striped table-bordered table-hover"
						id="dataTables-example">
						<thead>
							<tr>
								<th>Sl No</th>
								<th>Key Name</th>
								<th>Key Type</th>
								<th>Delete</th>
							</tr>
						</thead>
						<tbody>
							<%
								for (int i = 0; i < keyModels.size(); i++) {
							%>
							<tr>
								<td><%=i + 1%></td>
								<td><%=keyModels.get(i).getKeyName()%></td>
								<td><%=keyModels.get(i).getKeyType()%></td>
								<td><a
									href="delete-key/<%=keyModels.get(i).getKeyType()%>/<%=keyModels.get(i).getKeyName()%>" />Delete</a></td>
							</tr>
							<%
								}
							%>
						</tbody>
					</table>
					<%} else { 
if(generalException != null) {
%>
<%=generalException %>
<% } }  %>
				</div>
				<!-- /.table-responsive -->
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<%@ include file="footer.jsp"%>