<%@ include file="header.jsp"%>
<%@page import="java.util.*" %>
<%@page import="hemeraimagestore.model.*" %>

<div class="row">
<br><br>
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Update Metadata Details</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="dataTable_wrapper">
				<%
UpdateMetadataModel  updateMetaData = (UpdateMetadataModel)request.getAttribute("UpdateMetaData");
	
%>
					<table class="table table-striped table-bordered table-hover"
						id="dataTables-example">
						<thead>
		<tr>
			<th>Appliance Name</th>
			<th>Checksum</th>
			<th>To Version</th>
			<th>From Version</th>
			<th>Download Size</th>
		</tr>
						</thead>
						<tbody>
	<tr>
	<td><%=updateMetaData.getAppliance_name()  %></td>
	<td><%=updateMetaData.getChecksum()  %></td>
	<td><%=updateMetaData.getTo_version()  %></td>
	<td><%=updateMetaData.getFrom_version() %></td>
	<td><%=updateMetaData.getDownload_size() %></td>
	</tr>
						</tbody>
					</table>
				</div>
				<!-- /.table-responsive -->
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<%@ include file="footer.jsp"%>