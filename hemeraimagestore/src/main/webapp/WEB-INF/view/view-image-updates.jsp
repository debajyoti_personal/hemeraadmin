<%@ include file="header.jsp"%>
<%@page import="java.util.*" %>
<%@page import="hemeraimagestore.model.*" %>

<div class="row">
<br><br>
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Update List Details</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="dataTable_wrapper">
				<%
				List<UpdateModel> updateModels=(List<UpdateModel>)request.getAttribute("updateList"); 

				
%>
					<table class="table table-striped table-bordered table-hover"
						id="dataTables-example">
						<thead>
  <tr>
    <th>Update Id</th>
    <th>Update Time</th>
    <th>Appliance Name</th>
    <th>From Version</th>
    <th>Download Size</th>
    <th>Status</th>
    <th>View</th>
    <th>Download</th>
  </tr>
						</thead>
						<tbody>
							<%
							for(int i=0;i<updateModels.size();i++){
							%>
	<tr>
	<td><%=updateModels.get(i).getUpdate_id() %></td>
	<td><%=updateModels.get(i).getUpload_time() %></td>
	<td><%=updateModels.get(i).getAppliance_name() %></td>
	<td><%=updateModels.get(i).getFrom_version() %></td>
	<td><%=updateModels.get(i).getDownload_size() %></td>
	<td><%=updateModels.get(i).getStatus() %></td>
	<td><a href="/view-update?appliance_name=<%=updateModels.get(i).getAppliance_name()%>&version=<%=updateModels.get(i).getVersion() %>&from_version=<%=updateModels.get(i).getFrom_version()%>">View</a></td>
	<td><a href="/download-update/<%=updateModels.get(i).getUpdate_id()%>">Download</a></td>
	</tr>
							<%
								}
							%>
						</tbody>
					</table>
				</div>
				<!-- /.table-responsive -->
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<%@ include file="footer.jsp"%>