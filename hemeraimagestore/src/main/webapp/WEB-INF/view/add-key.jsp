<%@ include file="header.jsp"%>
<div class="row">
<br><br>
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Key List Details</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="col-lg-6">
				${error } ${message } ${error1 } ${message1 }
				<form action="insert-key" method="post" role="form">

					<div class="form-group">
						<label>Select Key Type</label> <select class="form-control" name="keyType" id="keyTypeId">
							<option value="0">--- Select ---</option>
							<option value="DEVICE">--- DEVICE ---</option>
							<option value="UPLOADER">--- UPLOADER ---</option>
						</select>
					</div>

					<div class="form-group">
						<label>Enter Key Name</label> <input class="form-control"
							placeholder="Enter Key" name="keyName">
					</div>

					<button type="submit" class="btn btn-default">Save</button>
				</form>
			</div>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<%@ include file="footer.jsp"%>