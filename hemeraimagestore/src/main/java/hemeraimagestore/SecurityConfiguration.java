package hemeraimagestore;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebMvcSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired 
	CustomAuthenticationProvider authProvider; 
    
  
 
    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	http
    		.csrf().disable()  //TODO: revisit
    		.authorizeRequests()
    		.antMatchers("/resources/**").permitAll()
    		.antMatchers("/**").access("hasRole('ROLE_USER')");
    		
    		
    	
    	http
          .formLogin().failureUrl("/login?error")
          .defaultSuccessUrl("/")
          .loginPage("/login")
          .permitAll()
          .usernameParameter("username")
          .passwordParameter("password")
          .and()
          .logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/login")
          .permitAll();
    }
    
    @Autowired
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
    	auth.authenticationProvider(authProvider);
    }

    
    
    
}
