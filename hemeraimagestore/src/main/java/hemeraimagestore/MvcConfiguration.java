package hemeraimagestore;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;


@Configuration
@ComponentScan(basePackages="hemeraimagestore")
@EnableWebMvc
public class MvcConfiguration extends WebMvcConfigurerAdapter{
	
    @Bean
    public ViewResolver getViewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/WEB-INF/view/");
        resolver.setSuffix(".jsp");
        
        return resolver;
    }
    
    // equivalents for <mvc:resources/> tags
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/css/**").addResourceLocations("/resources/META-INF/resources/css/").setCachePeriod(31556926);
        registry.addResourceHandler("/dist/**").addResourceLocations("/resources/META-INF/resources/dist/").setCachePeriod(31556926);
        registry.addResourceHandler("/js/**").addResourceLocations("/resources/META-INF/resources/js/").setCachePeriod(31556926);
        registry.addResourceHandler("/less/**").addResourceLocations("/resources/META-INF/resources/less/").setCachePeriod(31556926);
        registry.addResourceHandler("/bower_components/**").addResourceLocations("/resources/META-INF/resources/bower_components/").setCachePeriod(31556926);
    }

    @Override
    public void configureDefaultServletHandling(
            DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }  
    



    
}