package hemeraimagestore;

import hemeraimagestore.service.CustomHttpClient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Component;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {
	final PasswordEncoder encoder = new BCryptPasswordEncoder();

	// @Autowired
	// UserService userService;

	@Autowired
	CustomHttpClient customHttpClient;

	@Value("${spring.input.ApiLink}")
	private String apiLink;

	public CustomAuthenticationProvider() {
		super();
	}

	// API

	@Override
	public Authentication authenticate(final Authentication authentication)
			throws AuthenticationException {
		String url = apiLink + "login";
		final String password = authentication.getCredentials().toString();
		final String name = authentication.getName();// authentication.getName();
		HttpClient client = customHttpClient.getInstance();
		PostMethod postMethod = new PostMethod(url);
		postMethod.setParameter("username", name);
		postMethod.setParameter("password", password);
		int responseCodeFromServer = 0;

		try {
			client.executeMethod(postMethod);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		responseCodeFromServer = new Integer(postMethod.getStatusCode());

		if (responseCodeFromServer == 200) {
			final List<GrantedAuthority> grantedAuths = new ArrayList<>();
			grantedAuths.add(new SimpleGrantedAuthority("ROLE_USER"));

			UserDetails user = new User(name, password, grantedAuths);
			final Authentication auth = new UsernamePasswordAuthenticationToken(
					user, password, grantedAuths);
			SecurityContextHolder.getContext().setAuthentication(auth);
			return auth;
		} else {
			return null;
		}
	}

	@Override
	public boolean supports(final Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

}