package hemeraimagestore.service;

import hemeraimagestore.model.UserModel;

import java.io.IOException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.PostMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

	@Autowired
	CustomHttpClient customHttpClient; 
	
	public boolean loginValidate(UserModel userModel, String apiLink) throws HttpException, IOException{
		String url = apiLink + "login";
		System.out.println("url: "+ url);
		/*
		UserModel user = restTemplate.getForObject(url, UserModel.class, userModel.getUserName(), userModel.getPassword());
		
		System.out.println("user: "+ user);*/
		HttpClient client =customHttpClient.getInstance();
		//HttpClient client = new HttpClient();
        PostMethod postMethod = new PostMethod(url);
        postMethod.setParameter("username",userModel.getUserName());
        postMethod.setParameter("password", userModel.getPassword());
        
        int responseCodeFromServer = 0;
        
			client.executeMethod(postMethod);
			responseCodeFromServer = new Integer(postMethod.getStatusCode());
			System.out.println("responseCodeFromServer : "+ responseCodeFromServer);
			
			if(responseCodeFromServer == 200){
				return true;
				
			}
			else{
				return false;
			}
		
      }
}
