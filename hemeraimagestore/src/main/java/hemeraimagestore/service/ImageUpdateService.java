package hemeraimagestore.service;

import hemeraimagestore.model.ImageMetadataModel;
import hemeraimagestore.model.ImageModel;
import hemeraimagestore.model.UpdateMetadataModel;
import hemeraimagestore.model.UpdateModel;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.HeadMethod;
import org.apache.commons.httpclient.methods.OptionsMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Service
public class ImageUpdateService {

	@Value("${spring.input.ApiLink}")
	private String ApiLink;

	@Autowired
	CustomHttpClient customHttpClient;

	public int deleteImage(String link) throws HttpException, IOException {
		String url = ApiLink + link;
		HttpClient client = customHttpClient.getInstance();
		DeleteMethod getmethod = new DeleteMethod(url);
		int responseCodeFromServer = 0;
		client.executeMethod(getmethod);
		responseCodeFromServer = new Integer(getmethod.getStatusCode());
		return responseCodeFromServer;
	}

	public ImageMetadataModel viewImage(String link) throws HttpException,
			IOException {
		String url = ApiLink + link;
		HttpClient client = customHttpClient.getInstance();
		OptionsMethod getmethod = new OptionsMethod(url);
		int responseCodeFromServer = 0;
		client.executeMethod(getmethod);
		responseCodeFromServer = new Integer(getmethod.getStatusCode());
		String responseFromServer = getmethod.getResponseBodyAsString();
		ObjectMapper mapper = new ObjectMapper();
		ImageMetadataModel imageMetadata = mapper.readValue(responseFromServer,
				ImageMetadataModel.class);
		return imageMetadata;
	}

	public List<ImageModel> getImageList() throws HttpException, IOException {
		String url = ApiLink + "images/";
		HttpClient client = customHttpClient.getInstance();
		GetMethod getmethod = new GetMethod(url);
		int responseCodeFromServer = 0;
		List<ImageModel> responseFromServer = new ArrayList<ImageModel>();
		client.executeMethod(getmethod);
		responseCodeFromServer = new Integer(getmethod.getStatusCode());
		responseFromServer = new Gson().fromJson(
				new String(getmethod.getResponseBody()),
				new TypeToken<List<ImageModel>>() {
				}.getType());
		return responseFromServer;
	}

	public String getImageList(String appliance_name) throws HttpException,
			IOException {
		// TODO Auto-generated method stub
		String url = ApiLink + "images/";
		System.out.println("url: " + url + appliance_name);
		HttpClient client = customHttpClient.getInstance();
		GetMethod getmethod = new GetMethod(url);

		int responseCodeFromServer = 0;
		String responseFromServer = "";

		client.executeMethod(getmethod);
		responseCodeFromServer = new Integer(getmethod.getStatusCode());
		responseFromServer = new String(getmethod.getResponseBody());

		if (responseCodeFromServer == 200) {
			return responseFromServer;
		} else {
			return responseFromServer;
		}
	}

	public List<UpdateModel> getUpdateList() throws HttpException, IOException {
		String url = ApiLink + "updates/";
		HttpClient client = customHttpClient.getInstance();
		GetMethod getmethod = new GetMethod(url);
		int responseCodeFromServer = 0;
		List<UpdateModel> responseFromServer = new ArrayList<UpdateModel>();
		client.executeMethod(getmethod);
		responseCodeFromServer = new Integer(getmethod.getStatusCode());
		responseFromServer = new Gson().fromJson(
				new String(getmethod.getResponseBody()),
				new TypeToken<List<UpdateModel>>() {
				}.getType());

		return responseFromServer;
	}

	public void download(HttpServletResponse response, String link)
			throws HttpException, IOException {

		HttpClient client = customHttpClient.getInstance();
		String url = ApiLink + link;
		GetMethod getmethod = new GetMethod(url);
		int responseCodeFromServer = 0;
		String responseFromServer = "";
		// List<UpdateModel> responseFromServer = new ArrayList<UpdateModel>();
		client.executeMethod(getmethod);
		// System.out.println("Gson Raw Data************"+new
		// String(getmethod.getResponseBody()));
		responseCodeFromServer = new Integer(getmethod.getStatusCode());
		responseFromServer = new String(getmethod.getResponseBody());
		Header fileNameHeader = getmethod.getResponseHeader("fileName");
		String fileName = fileNameHeader.getValue();
		response.addHeader("Content-Disposition", "attachment; filename="
				+ fileName);
		InputStream is = new ByteArrayInputStream(getmethod.getResponseBody());
		org.apache.commons.io.IOUtils.copy(is, response.getOutputStream());
		response.flushBuffer();
		response.setStatus(HttpServletResponse.SC_OK);

	}

	public UpdateMetadataModel viewUpdate(String link) throws HttpException,
			IOException {
		String url = ApiLink + link;
		HttpClient client = customHttpClient.getInstance();
		OptionsMethod getmethod = new OptionsMethod(url);
		int responseCodeFromServer = 0;
		client.executeMethod(getmethod);
		responseCodeFromServer = new Integer(getmethod.getStatusCode());

		String responseFromServer = getmethod.getResponseBodyAsString();
		ObjectMapper mapper = new ObjectMapper();
		UpdateMetadataModel updateMetadata = mapper.readValue(
				responseFromServer, UpdateMetadataModel.class);

		return updateMetadata;

	}

	public List<UpdateModel> getUpdatesList(String link) throws HttpException,
			IOException {
		String url = ApiLink + link;
		HttpClient client = customHttpClient.getInstance();
		GetMethod getmethod = new GetMethod(url);
		int responseCodeFromServer = 0;
		List<UpdateModel> responseFromServer = new ArrayList<UpdateModel>();
		client.executeMethod(getmethod);
		responseCodeFromServer = new Integer(getmethod.getStatusCode());
		responseFromServer = new Gson().fromJson(
				new String(getmethod.getResponseBody()),
				new TypeToken<List<UpdateModel>>() {
				}.getType());

		return responseFromServer;
	}

}
