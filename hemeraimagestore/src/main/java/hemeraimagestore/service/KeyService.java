package hemeraimagestore.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import hemeraimagestore.model.KeyModel;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class KeyService {

	@Value("${spring.input.ApiLink}")
	private String ApiLink;

	@Autowired
	CustomHttpClient customHttpClient;

	public boolean insertKey(KeyModel keyModel) throws HttpException,
			IOException {

		String responseFromServer = "";
		int responseCodeFromServer = 0;
		boolean flag = false;

		if (keyModel.getKeyType().equalsIgnoreCase("DEVICE")) {
			String url = ApiLink + "devices/keys/" + keyModel.getKeyName();
			HttpClient client = CustomHttpClient.getInstance();
			PostMethod postmethod = new PostMethod(url);
			client.executeMethod(postmethod);
			responseCodeFromServer = new Integer(postmethod.getStatusCode());
			responseFromServer = new String(postmethod.getResponseBody());
		} else {
			String url = ApiLink + "uploaders/keys/" + keyModel.getKeyName();
			HttpClient client = CustomHttpClient.getInstance();
			PostMethod postmethod = new PostMethod(url);
			client.executeMethod(postmethod);
			responseCodeFromServer = new Integer(postmethod.getStatusCode());
			responseFromServer = new String(postmethod.getResponseBody());
		}
		if (responseCodeFromServer == 200)
			flag = true;
		else
			flag = false;

		return flag;

	}

	public List<KeyModel> viewApiKeys() throws HttpException, IOException {

		String responseFromServer = "";
		int responseCodeFromServer = 0;
		List<String> device_keys = new ArrayList<String>();
		List<String> uploader_keys = new ArrayList<String>();
		List<KeyModel> keyModelList = new ArrayList<KeyModel>();

		String deviceurl = ApiLink + "devices/keys/";
		String uploaderurl = ApiLink + "uploaders/keys/";
		HttpClient client = CustomHttpClient.getInstance();

		GetMethod getmethodForDevice = new GetMethod(deviceurl);

		client.executeMethod(getmethodForDevice);
		responseCodeFromServer = new Integer(getmethodForDevice.getStatusCode());
		responseFromServer = new String(getmethodForDevice.getResponseBody());
		if (!responseFromServer.replaceAll("\"", "").replace("[", "")
				.replace("]", "").trim().isEmpty()) {
			device_keys = Arrays.asList(responseFromServer.replaceAll("\"", "")
					.replace("[", "").replace("]", "").split("\\s*,\\s*"));
		}
		if (!device_keys.isEmpty()) {
			for (int i = 0; i < device_keys.size(); i++) {
				System.out.println(device_keys.get(i));
				KeyModel keyModel = new KeyModel();
				keyModel.setKeyName(device_keys.get(i));
				keyModel.setKeyType("DEVICE");
				keyModelList.add(keyModel);
			}
		}
		GetMethod getmethodForUploader = new GetMethod(uploaderurl);

		client.executeMethod(getmethodForUploader);
		responseCodeFromServer = new Integer(
				getmethodForUploader.getStatusCode());
		responseFromServer = new String(getmethodForUploader.getResponseBody());
		if (!responseFromServer.replaceAll("\"", "").replace("[", "")
				.replace("]", "").trim().isEmpty()) {
			uploader_keys = Arrays.asList(responseFromServer
					.replaceAll("\"", "").replace("[", "").replace("]", "")
					.split("\\s*,\\s*"));
		}

		if (!uploader_keys.isEmpty()) {
			for (int i = 0; i < uploader_keys.size(); i++) {
				KeyModel keyModel = new KeyModel();
				keyModel.setKeyName(uploader_keys.get(i));
				keyModel.setKeyType("UPLOADER");
				keyModelList.add(keyModel);
			}
		}
		return keyModelList;

	}

	public boolean deleteKey(KeyModel keyModel) throws HttpException,
			IOException {

		String responseFromServer = "";
		int responseCodeFromServer = 0;
		boolean flag = false;

		HttpClient client = CustomHttpClient.getInstance();

		if (keyModel.getKeyType().equalsIgnoreCase("DEVICE")) {
			String url = ApiLink + "devices/keys/" + keyModel.getKeyName();

			DeleteMethod deletemethodfordevice = new DeleteMethod(url);

			client.executeMethod(deletemethodfordevice);
			responseCodeFromServer = new Integer(
					deletemethodfordevice.getStatusCode());
			responseFromServer = new String(
					deletemethodfordevice.getResponseBody());
		} else {
			String url = ApiLink + "uploaders/keys/" + keyModel.getKeyName();

			DeleteMethod deletemethodforuplaoder = new DeleteMethod(url);

			client.executeMethod(deletemethodforuplaoder);
			responseCodeFromServer = new Integer(
					deletemethodforuplaoder.getStatusCode());
			responseFromServer = new String(
					deletemethodforuplaoder.getResponseBody());
		}
		if (responseCodeFromServer == 200)
			flag = true;
		else
			flag = false;

		return flag;
	}
}
