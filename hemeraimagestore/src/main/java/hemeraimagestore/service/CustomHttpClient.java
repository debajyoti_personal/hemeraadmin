package hemeraimagestore.service;

import org.apache.commons.httpclient.HttpClient;
import org.springframework.stereotype.Component;

@Component
public class CustomHttpClient {
	private static HttpClient httpClient;
	
	public static HttpClient getInstance(){
		if(httpClient==null){
			httpClient=new HttpClient();
		}
		return httpClient;
		
	}
}
