package hemeraimagestore.controllers;

import java.io.IOException;
import java.util.List;

import hemeraimagestore.model.KeyModel;
//import hemeraimagestore.service.KeyService;

import hemeraimagestore.service.KeyService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.httpclient.HttpException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@Configuration
@EnableAutoConfiguration
public class KeyController {
	
    @Autowired
    private KeyService keyService;

    @Value("${spring.input.ApiLink}")
    private String ApiLink;
    
	@RequestMapping(value = "/add-api-keys",method = {RequestMethod.POST,RequestMethod.GET})
	public String addKeyForm(Model model,HttpServletRequest httpServletRequest){
			
		return "add-key";
	}
	
	@RequestMapping(value = "/insert-key",method = {RequestMethod.POST,RequestMethod.GET})
	public String insertRoleLevelData(@ModelAttribute KeyModel keyModel,Model model,HttpServletRequest httpServletRequest,final RedirectAttributes redirectAttributes,HttpSession httpSession) {
		
		System.out.println("in insertKey controller :: "+keyModel.getKeyName()+"  "+keyModel.getKeyType());
        try {
			boolean response = keyService.insertKey(keyModel);
			if(response)
				model.addAttribute("message", "API Key successfully saved");
			else
				model.addAttribute("error", "API key saving failed");
			System.out.println(response);
		} catch (HttpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	    return "add-key";
			
		}
	
	@RequestMapping(value = "/view-api-keys",method = {RequestMethod.GET})
	public String viewApiKeys(Model model,HttpServletRequest httpServletRequest,final RedirectAttributes redirectAttributes,HttpSession httpSession) {
		
		System.out.println("in viewApiKeys controller");
        try {
        	List<KeyModel> keyModelList = keyService.viewApiKeys();
			if(keyModelList != null)
				model.addAttribute("keyModelList", keyModelList);
			else
				model.addAttribute("error", "No keys found");
			System.out.println(keyModelList.size());
		} catch (HttpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	    return "view-key";
			
		}
	
	@RequestMapping(value = "/delete-key/{key_type}/{key_name}",method = {RequestMethod.GET})
	public String deleteApiKey(Model model,HttpServletRequest httpServletRequest,final RedirectAttributes redirectAttributes,@PathVariable String key_type, @PathVariable String key_name) {
		
		System.out.println("in deleteKey controller ");
		KeyModel keyModel = new KeyModel();
		keyModel.setKeyName(key_name);
		keyModel.setKeyType(key_type);
        try {
			boolean response = keyService.deleteKey(keyModel);
			if(response)
				redirectAttributes.addAttribute("message1", "API Key successfully deleted");
			else
				redirectAttributes.addAttribute("error1", "API key delete failed");
			System.out.println(response);
		} catch (HttpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
	    return "redirect:/view-api-keys";
			
		}

	
	

}
