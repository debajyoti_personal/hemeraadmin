package hemeraimagestore.controllers;

import hemeraimagestore.model.ImageMetadataModel;
import hemeraimagestore.model.ImageModel;
import hemeraimagestore.model.UpdateMetadataModel;
import hemeraimagestore.model.UpdateModel;
import hemeraimagestore.service.ImageUpdateService;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.HttpException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
@Configuration
@EnableAutoConfiguration
@RequestMapping(value = "/")
public class ImageAndUpdateController {
	
    @Autowired
    private ImageUpdateService imageUpdateService;
    
    
	@RequestMapping(value="", method=RequestMethod.GET)
	public String loginProcess(Model model){
		User user = (User) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		model.addAttribute("userName",user.getUsername());

		return "login-home";

	}
	
	@RequestMapping(value = "/view-images",method = {RequestMethod.POST,RequestMethod.GET})
	public String viewImages(Model model,HttpServletRequest httpServletRequest) throws HttpException, IOException{
		List<ImageModel> imagesList=imageUpdateService.getImageList();
		
		model.addAttribute("imageList",imagesList);
		
		return "view-image-list";
	}
	
/*	@RequestMapping(value = "/view-update/{id:.+}",method = {RequestMethod.POST,RequestMethod.GET})
	public String viewUpdate(Model model,HttpServletRequest httpServletRequest,@PathVariable String id) throws HttpException, IOException{
		String link="images/by_id/"+id;
		String response=imageUpdateService.viewImage(link);
		model.addAttribute("response",response);
		System.out.println("8888888888"+response);
		return "view-update";
	}*/
	
	@RequestMapping(value = "/delete-images/{app_name}/{version:.+}",method = {RequestMethod.POST,RequestMethod.GET})
	public String DeleteImages(Model model,HttpServletRequest httpServletRequest,@PathVariable String app_name,@PathVariable String version) throws HttpException, IOException{
		String link="images/"+app_name+"/"+version;
		int status=imageUpdateService.deleteImage(link);
		return "redirect:/view-images";
	}
	
/*	@RequestMapping(value = "/view-image/{id:.+}",method = {RequestMethod.POST,RequestMethod.GET})
	public String viewImages(Model model,HttpServletRequest httpServletRequest,@PathVariable String id) throws HttpException, IOException{
		String link="images/by_id/"+id;
		String response=imageUpdateService. viewImage(link);
		model.addAttribute("response",response);
		return "view-image-detail";
}*/
	
		
	@RequestMapping(value = "/view-image",method = {RequestMethod.POST,RequestMethod.GET})
	public String viewImage(@RequestParam("appliance_name") String appliance_name ,Model model,HttpServletRequest httpServletRequest,@RequestParam("version") String version) throws HttpException, IOException {
		
		String link="images/"+appliance_name+"/"+version;		
		System.out.println("inside view-update controller");
		System.out.println(link+"link");
		ImageMetadataModel ImageMetaData = imageUpdateService.viewImage(link);    	
		model.addAttribute("ImageMetaData",ImageMetaData);
		return "view-image";
			
		}

	@RequestMapping(value = "/view-updates",method = {RequestMethod.POST,RequestMethod.GET})
	public String viewUpdates(Model model,HttpServletRequest httpServletRequest) throws HttpException, IOException{
		System.out.println("I am in Controller");
		List<UpdateModel> updatesList=imageUpdateService.getUpdateList();
		model.addAttribute("updateList",updatesList);
		
		return "view-update-list";
	}
	
	@RequestMapping(value = "/view-update",method = {RequestMethod.POST,RequestMethod.GET})
	public String viewUpdate(Model model,HttpServletRequest httpServletRequest,@RequestParam("appliance_name") String appliance_name,@RequestParam("version") String version,@RequestParam("from_version") String from_version) throws HttpException, IOException{
		String link="updates/"+appliance_name+"/"+version+"?from_version="+from_version;		
		System.out.println("inside view-update controller");
		System.out.println(link+"link");
		UpdateMetadataModel UpdateMetaData = imageUpdateService.viewUpdate(link);    	
		model.addAttribute("UpdateMetaData",UpdateMetaData);
		return "view-update";
}
	
	@RequestMapping(value = "/download-update/{id}",method = {RequestMethod.POST,RequestMethod.GET})
	public String downloadUpdate(Model model,HttpServletResponse response,@PathVariable String id) throws HttpException, IOException{
		String link="updates/by_id/"+id;
		System.out.println(link);
		 imageUpdateService.download(response,link);
		 
		return "redirect:/view-updates";
}
	
	@RequestMapping(value = "/download-image",method = {RequestMethod.POST,RequestMethod.GET})
	public String downloadImage(Model model,HttpServletResponse response,@RequestParam("appliance_name") String appliance_name,@RequestParam("version") String version) throws HttpException, IOException{
		String link="images/"+appliance_name+"/"+version;
		System.out.println(link);
		 imageUpdateService.download(response,link);
		 
		return "redirect:/view-images";
}

	@RequestMapping(value = "/view-image-updates",method = {RequestMethod.POST,RequestMethod.GET})
	public String viewUpdatesByApplianceName(Model model,HttpServletRequest httpServletRequest,@RequestParam("appliance_name") String appliance_name) throws HttpException, IOException{
		System.out.println("I am in Controller");
		String link="updates/"+appliance_name;
		List<UpdateModel> updatesList=imageUpdateService.getUpdatesList(link);
		
		model.addAttribute("updateList",updatesList);
		
		return "view-image-updates";
	}

}
