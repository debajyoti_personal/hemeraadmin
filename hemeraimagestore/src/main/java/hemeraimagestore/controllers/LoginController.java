package hemeraimagestore.controllers;

import hemeraimagestore.model.UserModel;
import hemeraimagestore.service.UserService;

import java.io.IOException;

import org.apache.commons.httpclient.HttpException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@Configuration
@EnableAutoConfiguration
public class LoginController {

	@Autowired
	private UserService userService;
	
	@Value("${spring.input.ApiLink}")
    private String ApiLink;
	
	/**
	 * 
	 * @return
	 */
	@RequestMapping(value="/login", method=RequestMethod.GET)
	public String login(){
		System.out.println("login-start");
		return "login";
	}
	
	/**
	 * 
	 * @param user
	 * @param model
	 * @return
	 */
//	@RequestMapping(value="/login", method=RequestMethod.POST)
//	public String loginProcess(@ModelAttribute UserModel user ,Model model){
//		
//		System.out.println("username: "+ user.getUserName());
//		System.out.println("password: "+ user.getPassword());
//		
//		boolean flag = false;
//		try {
//			flag = userService.loginValidate(user, ApiLink);
//			System.out.println("flag: "+ flag);
//		} catch (HttpException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		if(!flag){
//			model.addAttribute("error", "Invalid Username or Password!");
//			return "login";
//		}
//		else{
//			model.addAttribute("userName", user.getUserName());
//			return "login-home";
//		}
//		
//	}

	
	
}
