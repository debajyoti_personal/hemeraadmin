package hemeraimagestore.model;

import org.springframework.stereotype.Component;

@Component
public class KeyModel {
	
	private String keyType;
	private String keyName;
	
	public String getKeyType() {
		return keyType;
	}
	public void setKeyType(String keyType) {
		this.keyType = keyType;
	}
	public String getKeyName() {
		return keyName;
	}
	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}
	
	

}
