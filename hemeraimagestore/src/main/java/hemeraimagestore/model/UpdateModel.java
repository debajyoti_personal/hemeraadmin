package hemeraimagestore.model;

import org.springframework.stereotype.Component;

@Component
public class UpdateModel {

	private String update_id;
	private String upload_time;
	private String appliance_name;
	private String from_version;
	private String type;
	private String version;
	private String download_size;
	private String status;
	public String getUpdate_id() {
		return update_id;
	}
	public void setUpdate_id(String update_id) {
		this.update_id = update_id;
	}
	public String getUpload_time() {
		return upload_time;
	}
	public void setUpload_time(String upload_time) {
		this.upload_time = upload_time;
	}
	public String getAppliance_name() {
		return appliance_name;
	}
	public void setAppliance_name(String appliance_name) {
		this.appliance_name = appliance_name;
	}
	public String getFrom_version() {
		return from_version;
	}
	public void setFrom_version(String from_version) {
		this.from_version = from_version;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getDownload_size() {
		return download_size;
	}
	public void setDownload_size(String download_size) {
		this.download_size = download_size;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
}
