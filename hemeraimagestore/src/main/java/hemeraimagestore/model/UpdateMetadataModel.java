package hemeraimagestore.model;

import org.springframework.stereotype.Component;

@Component
public class UpdateMetadataModel {
	
	private String appliance_name;
	private String checksum;
	private String to_version;
	private String from_version;
	private String download_size;
	
	public String getAppliance_name() {
		return appliance_name;
	}
	public void setAppliance_name(String appliance_name) {
		this.appliance_name = appliance_name;
	}
	public String getChecksum() {
		return checksum;
	}
	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}
	public String getTo_version() {
		return to_version;
	}
	public void setTo_version(String to_version) {
		this.to_version = to_version;
	}
	public String getFrom_version() {
		return from_version;
	}
	public void setFrom_version(String from_version) {
		this.from_version = from_version;
	}
	public String getDownload_size() {
		return download_size;
	}
	public void setDownload_size(String download_size) {
		this.download_size = download_size;
	}
	
	

}
