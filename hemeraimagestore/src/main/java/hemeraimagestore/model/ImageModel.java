package hemeraimagestore.model;

import org.springframework.stereotype.Component;

@Component
public class ImageModel {

	private String upload_time;
	private String appliance_name;
	private String image_id;
	private String version;
	private String download_size;
	private String status;
	
	public String getUpload_time() {
		return upload_time;
	}
	public void setUpload_time(String upload_time) {
		this.upload_time = upload_time;
	}
	public String getAppliance_name() {
		return appliance_name;
	}
	public void setAppliance_name(String appliance_name) {
		this.appliance_name = appliance_name;
	}
	public String getImage_id() {
		return image_id;
	}
	public void setImage_id(String image_id) {
		this.image_id = image_id;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getDownload_size() {
		return download_size;
	}
	public void setDownload_size(String download_size) {
		this.download_size = download_size;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
}
