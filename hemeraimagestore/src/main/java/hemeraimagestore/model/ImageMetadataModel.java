package hemeraimagestore.model;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class ImageMetadataModel {

	private String has_installer;
	private String appliance_name;
	private String checksum;
	private String[] packages;
	private String version;
	private String download_size;
	
	public String getHas_installer() {
		return has_installer;
	}
	public void setHas_installer(String has_installer) {
		this.has_installer = has_installer;
	}
	public String getAppliance_name() {
		return appliance_name;
	}
	public void setAppliance_name(String appliance_name) {
		this.appliance_name = appliance_name;
	}
	public String getChecksum() {
		return checksum;
	}
	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}
		
	public String[] getPackages() {
		return packages;
	}
	public void setPackages(String[] packages) {
		this.packages = packages;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getDownload_size() {
		return download_size;
	}
	public void setDownload_size(String download_size) {
		this.download_size = download_size;
	}
	
	
}
